"use strict";

let navList = document.querySelector(".tabs");
let contentList = document.querySelector(".tabs-content");

for (let iter = 0; iter < navList.children.length; iter++) {
  navList.children[iter].classList.add("nav-id-" + (iter + 1));
}

navList.addEventListener("click", function (e) {
  let buttonTarget = e.target;
  document.querySelector(".active").classList.remove("active");
  buttonTarget.classList.add("active");
  document.querySelector(".tabs-content li.active").classList.remove("active");
  contentList.children[buttonTarget.classList[1].slice(7) - 1].classList.add(
    "active"
  );
});
